import java.io.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * This class provides methods used to handle with shared objects on client's side.
 * 
 * @author Jérémy RIVIERE
 * @author Vincent ANGLADON
 * 
 * @see SharedObject_itf
 */

public class SharedObject implements Serializable, SharedObject_itf {
	
	private static final long serialVersionUID = -1500934351853665607L;
	
	/**
	 * The object shared.
	 */
	public Object obj;
	
	/**
	 * The actual lock status of the object.
	 */
	private LocalLock lock;
	
	/**
	 * The id associated to the object.
	 */
	public int id;
	
	/**
	 * Logger for SharedObject.
	 */
	private static final Logger logger = Logger.getLogger("SharedObject.class"); 
	
	/**
	 * The state of the Shared object
	 */
	private State state;
	
	/**
	 * Previous method called
	 */
	private String previousCall;
	
	/**
	 * Lock to protect concurrent access to this.lock
	 */
	private  Lock verrou;
	
	/**
	 * Lock to synchronize methods inside the shared object
	 */
	private Integer m_lock;
	
	/**
	 * Create a new SharedObject used to manage lock level without having to send a request to the server.
	 * @param obj The object to be shared.
	 * @param id The id associated to this SharedObject, as given by the server at its creation.
	 */
	public SharedObject(Object obj, int id) {
		this.obj = obj;
		lock = LocalLock.NL;
		this.id = id;
		DOMConfigurator.configure("log4j.xml");
//		try {
//			logger.addAppender(new FileAppender(new PatternLayout("%d{ABSOLUTE} %-5p %c - %m%n"), Client.clientID+"_SharedObject.log"));
//		} catch (IOException e1) {
//			System.err.println("Impossible to create the layout");
//		}
		state = State.free;
		previousCall = "unknown";
		verrou = new ReentrantLock();
		m_lock = new Integer(0);
	}
	
	/**
	 * Invoked by the user program on the client node, to request a read lock.
	 */
	public void lock_read() {
		logger.trace(Client.clientID + " : Entered lock_read");
		if (state != State.free)
			logger.warn(Client.clientID+" : Appel de lock_read en simultané avec " + previousCall);
		state = State.busy;
		previousCall = "lock_read";
		verrou.lock();
		LocalLock oldLock = lock;
		switch (lock) {
		case NL:
			verrou.unlock();
			Object newObj = Client.lock_read(id); // Request a read lock to the server.
			if (newObj != null) {
				obj = newObj;
			}
			else {
				logger.error(Client.clientID+" : RPC call Client.lock_read failed (lock_read)");
				throw new RuntimeException("RPC call Client.lock_read failed");
			}
			verrou.lock();
			lock = LocalLock.RLT;
			break;
		case RLC:
			lock = LocalLock.RLT;
			break;
		case WLC:
			lock = LocalLock.RLT_WLC;
			break;
		default:
			logger.log(Level.ERROR, Client.clientID+" Invalid state (lock read)");
			break;
	
		}
		logger.log(Level.INFO, Client.clientID+" : "+oldLock.toString()+"-> "+lock.toString()+" (lock read from client)");
		verrou.unlock();
		state = State.free;

	}
	
	/**
	 * Invoked by the user program on the client node, to request a write lock.
	 */
	public void lock_write() {
		synchronized(m_lock) {
			logger.trace(Client.clientID + " : Entered lock_write");
			if (state != State.free)
				logger.warn(Client.clientID+" : Appel de lock_write en simultané avec " + previousCall);
			state = State.busy;
			previousCall = "lock_write";
			verrou.lock();
			LocalLock oldLock = lock;
			switch (lock) {
			case NL:
			case RLC:
				verrou.unlock();
				Object newObj = Client.lock_write(id); // Request a write lock to the server.
				if (newObj != null) {
					obj = newObj;
				}
				else {
					logger.error(Client.clientID+" : RPC call Client.lock_write failed (lock_write)");
					throw new RuntimeException("RPC call Client.lock_write failed");
				}
				verrou.lock();
			case WLC:
				lock = LocalLock.WLT; // No request, as the lock is cached.
			break;
			default:
				logger.log(Level.ERROR, Client.clientID+" : Invalid operation (lock write from client) when in state "+lock.toString());
				break;
			}
			logger.log(Level.INFO, Client.clientID+" : "+oldLock.toString()+"-> "+lock.toString()+" (lock write from client)");
			verrou.unlock();
			state = State.free;
		}
	}
	
	/**
	 * Invoked by the user program on the client node, to unlock the object.
	 */
	public void unlock() {
		logger.trace(Client.clientID + " : entered unlock");
		previousCall = "unlock";
		synchronized (this) {
			verrou.lock();
			LocalLock oldLock = lock;
			switch (lock) {
			case RLT:
				lock = LocalLock.RLC;
				break;
			case WLT:
			case RLT_WLC:
				lock = LocalLock.WLC;
				break;
			default:
				logger.log(Level.ERROR, Client.clientID+" : Invalid operation (unlock from client) when in state "+lock.toString());
				break;	
			}
			verrou.unlock();
			try {
				this.notify();
			} catch (Exception e) {
				logger.error(Client.clientID+" Failed to notify an unlock");
				e.printStackTrace();
			}
			logger.log(Level.INFO, Client.clientID+" : "+oldLock.toString()+"-> "+lock.toString()+" (unlock from client)");
		}
	}
	
	
	/**
	 * Callback invoked remotely by the server, to notify the writer to give his lock back, as other clients requested a read lock.
	 * @return The value of the object as modified by the writer.
	 */
	public Object reduce_lock() {
		logger.trace(Client.clientID + " : Entered reduce_lock");
		synchronized(m_lock) {
			synchronized (this) {
				if (state != State.free)
					logger.warn(Client.clientID+" : Appel de reduce_lock en simultané avec " + previousCall);
				state = State.busy;
				previousCall = "reduce_lock";
				verrou.lock();
				LocalLock oldLock = lock;
				while (lock.equals(LocalLock.WLT)) {
					verrou.unlock();
					// We wait until the object is unlocked.
					try {
						logger.trace(Client.clientID+" : Waiting for unlock (reduce_lock from server)");
						this.wait();
						logger.trace(Client.clientID+" end wait.");
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					verrou.lock();
				}
				switch (lock) {
				case WLT:
				case WLC:
					lock = LocalLock.RLC;
					break;
				case RLT_WLC:
					lock = LocalLock.RLT;
					break;
				default:
					logger.log(Level.ERROR, Client.clientID+" Useless query (reduce_lock from server) when in state "+lock.toString());
					break;
				}
				logger.log(Level.INFO, Client.clientID+" "+oldLock.toString()+"-> "+lock.toString()+" (reduce lock from server)");
				verrou.unlock();
				state = State.free;
				return obj;
			}
		}
	}
	
	/**
	 * Callback invoked remotely by the server, to notify a reader to give his lock back, as another client requested a write lock.
	 */
	public void invalidate_reader() {
		logger.trace(Client.clientID + " : Entered invalidate_reader");
		synchronized (this) {
			if (state != State.free)
				logger.warn(Client.clientID+" : Appel de invalidate_reader en simultané avec " + previousCall);
			state = State.busy;
			previousCall = "invalidate_reader";
			verrou.lock();
			LocalLock oldLock = lock;
			while (lock.equals(LocalLock.RLT) || lock.equals(LocalLock.RLT_WLC)) {
				verrou.unlock();
				// We wait until the object is unlocked.
				try {
					logger.trace(Client.clientID+" : Waiting for unlock (invalidate_reader from server)");
					this.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				verrou.lock();
			}
			switch (lock) {
			case RLT:
			case RLC:
				lock = LocalLock.NL;
				break;
			default:
				logger.log(Level.ERROR, Client.clientID+" : Useless querry (invalidate reader from server) when in state "+lock.toString());
				break;
			}
		logger.log(Level.INFO, Client.clientID+" "+oldLock.toString()+"-> "+lock.toString()+" (invalidate reader from server)");
		verrou.unlock();
		state = State.free;
		}
	}
	
	/**
	 * Callback invoked remotely by the server, to notify the reader to give his lock back, as another client requested a write lock.
	 */
	public Object invalidate_writer() {
		logger.trace(Client.clientID + " : Entered invalidate_writer");
		synchronized(m_lock) {
			synchronized (this) {
				if (state != State.free)
					logger.warn(Client.clientID+" : Appel de invalidate_writer en simultané avec " + previousCall);
				state = State.busy;
				previousCall = "invalidate_reader";
				verrou.lock();
				LocalLock oldLock = lock;
				while (lock.equals(LocalLock.WLT) || lock.equals(LocalLock.RLT_WLC)) {
					verrou.unlock();
					// We wait until the object is unlocked.
					try {
						logger.trace(Client.clientID+" : Waiting for unlock (invalidate_writer from server) current state is "+lock);
						this.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					verrou.lock();
				}
			    switch (lock) {
			    case WLT:
			    case RLT_WLC:
			    case WLC:
				    lock = LocalLock.NL;
			    	break;
			    default:
			    	logger.log(Level.ERROR, Client.clientID+" : Useless query (invalidate writer from server) when in state "+lock.toString());
			    	break;
			    }
			    logger.log(Level.INFO, Client.clientID+" "+oldLock.toString()+"-> "+lock.toString()+" (invalidate writer from server)");
			    verrou.unlock();
			    state = State.free;
				return obj;
			}
		}
	}
	
	/**
	 * 
	 * Deprecated, as the id is a public field of this class
	 */
	@Deprecated
	public int getId() {
		return id;
	}
	
	
	private enum State {
		busy,
		free
	}
}
