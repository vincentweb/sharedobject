import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;

import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.xml.DOMConfigurator;


public class Compteur implements Serializable {
	
	private static final Logger logger = Logger.getLogger("Compteur.class");
	
	private static final long serialVersionUID = -4492550325490126607L;
	
	Integer cpt;
	public HashMap<String, Integer> clients;
	
	public Compteur() {
		this(0);
	}
	
	public Compteur(int init) {
		cpt = new Integer(init);
		DOMConfigurator.configure("log4j.xml");
    	try {
			logger.addAppender(new FileAppender(new PatternLayout("%-5p %c - %m%n"), Client.clientID+"_Compteur.log"));
		} catch (IOException e1) {
			System.err.println("Impossible to create the layout");
		}
	}

	public static void main(String[] args) {
		String addr = "127.10.0.1";
		if (args.length >= 1) {
			addr = args[0];
		}
		Client.init(addr);
		SharedObject s = Client.lookup("CPT");
		if (s == null) {
			s = Client.create(new Compteur(0));
			Client.register("CPT", s);
		}
		s.lock_write();
		if (((Compteur)s.obj).clients==null) {
			((Compteur)s.obj).clients = new HashMap<String, Integer>();
		}
		((Compteur)s.obj).clients.put(Client.clientID, 0);
		s.unlock();
		for (int i = 0; i < 100; i++) {
			s.lock_read();
			System.out.println("Itération " + i + ": " + ((Compteur)s.obj).cpt);
			
			s.unlock();
			s.lock_write();
			((Compteur)s.obj).cpt++;
			((Compteur)s.obj).clients.put(Client.clientID, ((Compteur)s.obj).clients.get(Client.clientID)+1);
			logger.debug(Client.clientID + " - cpt : " + String.valueOf(((Compteur)s.obj).cpt));
			s.unlock();
		}
		s.lock_read();
		System.out.println("Last iteration: " + ((Compteur)s.obj).cpt);
		logger.debug("Last value : " + ((Compteur)s.obj).cpt +  "(" + Client.clientID + ")");
		s.unlock();
	}
	
	public String toString() {
		return String.valueOf(cpt);
	}
}
