import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * This class describes a SharedObject as known on server side.
 * 
 * @author Jérémy RIVIERE
 * @author Vincent ANGLADON
 *
 */

public class ServerObject {

	/**
	 * The id associated to this ServerObject.
	 */
	private int id;
	
	/**
	 * The object pointed to by the ServerObject.
	 */
	private Object object; 
	
	/**
	 * A reference to the client currently using the object in writing mode.
	 */
	private Client_itf writer;
	
	/**
	 * A list of all the clients currently reading the object.
	 */
	private ArrayList<Client_itf> readers;
	
	/**
	 * Logger for ServerObject.
	 */
	private static final Logger logger = Logger.getLogger("ServerObject.class");
	
//	private Lock verrou;
	
	
	public ServerObject(int id, Object object) {
		this.id = id;
		this.object = object;
		writer = null;
		readers = new ArrayList<Client_itf>();
		DOMConfigurator.configure("log4j.xml");
//		verrou = new ReentrantLock();
	
	}
	
	
	/**
	 * 
	 * Called by the server to request a read lock on an object.
	 * @param reader The client who requested the read lock.
	 * @return The actual state of the shared object.
	 */
	public synchronized Object lock_read(Client_itf reader) {
		String clientId = "Unknown";
//			try {
//				clientId = reader.getId();
//			} catch (RemoteException e1) {
//				e1.printStackTrace();
//			}
	
		
		if (writer != null) {
			// Try to reduce lock for the writer
			try {
				object = writer.reduce_lock(id);
				readers.add(writer);
				writer = null;
//				logger.trace(clientId + " " + reader.getId() + " reduced and added the writer");
			} catch (Exception e) {
				logger.error(clientId + " " + String.valueOf(id) + " Failed to reduce lock for "+String.valueOf(id)+"\n"+e.getMessage()+"\n"+e.fillInStackTrace());
				e.printStackTrace();
			}
		}
		readers.add(reader);
		logger.trace(clientId + " " + String.valueOf(id) + " exit lock_read, returns "+object.toString());
	
		return object;
	}
	
	/**
	 * 
	 * Called by the server to request a write lock on an object.
	 * @param writer The client who requested the write lock.
	 * @return The actual state of the shared object.
	 */
	public synchronized Object lock_write(Client_itf writer) {
//		verrou.lock();
		String clientId = "Unknown";
//			try {
//				clientId = writer.getId();
//			} catch (RemoteException e1) {
//				e1.printStackTrace();
//			}
	
		
		logger.debug("Entrée dans le lock_write");
		if (this.writer == null) {
			for (Client_itf client : readers) {
				// Try to invalidate all the readers
				try {
					if (!client.equals(writer)) {
						client.invalidate_reader(id);
					}
					logger.trace(clientId + " " + String.valueOf(id) + " Invalidate reader ");
				} catch (Exception e) {
					logger.error(clientId + " " + String.valueOf(id) + " Failed to invalidate reader "+String.valueOf(id)+"\n"+e.getStackTrace());
					e.printStackTrace();
				}
			}
			readers.clear();
			this.writer = writer;
		}
		else {
			// Try to invalidate the client who is actually using the object in writing mode. (There are necessarily no client in reading mode)
			try {
				object = this.writer.invalidate_writer(id);
				logger.trace(clientId + " " + String.valueOf(id) + " Invalidate writer");
			} catch (Exception e) {
				logger.error(clientId + " " + String.valueOf(id) + " Failed to invalidate writer "+String.valueOf(id)+"\n"+e.getStackTrace().toString());
				e.printStackTrace();
			}
			this.writer = writer;
		}
		logger.trace(clientId + " " + String.valueOf(id) + " exit lock_write, returns "+object.toString());
//		verrou.unlock();
		return object;
	}
	
	@Deprecated
	public int getId() {
		return id;
	}
}
