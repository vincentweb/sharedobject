
/**
 * Describes the flag values of a local lock
 * 
 * @author Jérémy RIVIERE
 * @author Vincent ANGLADON
 *
 */

public enum LocalLock {
	NL, // No Local Lock
	RLC, // Read Lock Cached: No need to ask the server for a new readLock
	WLC, // Write Lock Cached: No need to ask the server for a new writeLock
	RLT, // Read Lock Taken: No other application can access the SharedObject for writing
	WLT, // Write Lock Taken: No other application can access the SharedObject for writing nor reading
	RLT_WLC // Read Lock Taken and Write Lock Cached
}
