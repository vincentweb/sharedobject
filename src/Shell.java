// Time-stamp: <24 Jan 2006 10:30 queinnec@enseeiht.fr>

import java.util.Map;
import java.util.HashMap;
import java.io.*;

public class Shell {

    private interface Action {
        public void run (String[] args);
    }

    private static Map objectCache = new HashMap();

    private static SharedObject getObject (String objname) {
        SharedObject obj = (SharedObject) objectCache.get (objname);
        if (obj == null) {
            obj = Client.lookup (objname);
            if (obj == null)
              throw new Error ("Object "+objname+" not found.");
            objectCache.put (objname, obj);
        }
        return obj;
    }
    
    private static String previousName = null;
    private static String extractName (String[] a) {
        String name;
        if (a.length == 1) {
            name = previousName;
        } else {
            name = a[1];
            previousName = name;
        }
        return name;
    }

    public static void main(String[] args) throws Exception {

        Client.init();

        Map actions = new HashMap();

        actions.put ("help", new Action() {
                public void run (String[] args) {
                    System.out.println ("create name val");
                    System.out.println ("lock_write [name]");
                    System.out.println ("lock_read [name]");
                    System.out.println ("unlock [name]");
                    System.out.println ("get [name]");
                    System.out.println ("set [name] val");
                    System.out.println ("quit");
                }});
                
        actions.put ("create", new Action() {
                public void run (String[] args) {
                    SharedObject obj = Client.create (args[2]);
                    Client.register (args[1], obj);
                    objectCache.put (args[1], obj);
                    previousName = args[1];
                }});
        actions.put ("c", actions.get("create"));

        actions.put ("lock_write", new Action() {
                public void run (String[] args) {
                    String name = extractName (args);
                    SharedObject obj = getObject (name);
                    obj.lock_write();
                }});
        actions.put ("write", actions.get("lock_write"));
        actions.put ("w", actions.get("lock_write"));

        actions.put ("lock_read", new Action() {
                public void run (String[] args) {
                    String name = extractName (args);
                    SharedObject obj = getObject (name);
                    obj.lock_read();
                }});
        actions.put ("read", actions.get("lock_read"));
        actions.put ("r", actions.get("lock_read"));

        actions.put ("unlock", new Action() {
                public void run (String[] args) {
                    String name = extractName (args);
                    SharedObject obj = getObject (name);
                    obj.unlock();
                }});
        actions.put ("u", actions.get("unlock"));

        actions.put ("get", new Action() {
                public void run (String[] args) {
                    String name = extractName (args);
                    SharedObject obj = getObject (name);
                    System.out.println (name + " = " + obj.obj);
                }});
        actions.put ("g", actions.get("get"));

        actions.put ("set", new Action() {
                public void run (String[] args) {
                    if (args.length == 2) {
                        SharedObject obj = getObject (previousName);
                        obj.obj = args[1];
                    } else {
                        previousName = args[1];
                        SharedObject obj = getObject (args[1]);
                        obj.obj = args[2];
                    }
                }});
        actions.put ("s", actions.get("set"));

        actions.put ("quit", new Action() {
                public void run (String[] args) {
                    System.exit(0);
                }});
        actions.put ("q", actions.get("quit"));


        BufferedReader in = new BufferedReader (new InputStreamReader (System.in));
        while (true) {
            System.out.print ("> ");
            String line = in.readLine();
            if (line == null)
              break;
            String[] cmde = line.split("\\s");
            Action a = (Action) actions.get(cmde[0]);
            if (a != null) {
                try {
                    a.run (cmde);
                } catch (Throwable e) {
                    System.out.println ("Echec : "+e);
                }
            } else {
                System.out.println ("???");
            }
        }
        System.exit(0);
    }
}
