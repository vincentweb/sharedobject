import java.rmi.RemoteException;
import java.util.HashMap;


public class CheckCounter extends Thread {
	
	protected Object obj;
	
	public CheckCounter() {
		this.obj = null;
	}
	
	public void updateObject(Object obj) {
		this.obj = obj;
	}
	
	
	public void run() {
		while(true) {
			Sleeping.sleep(2000);
			if (obj != null && ((Compteur)obj).clients != null) {
				System.out.println("<-------- Rapport");
				for (String client : ((Compteur)obj).clients.keySet()) {
					System.out.println(String.format("Client %s \t %d",client,((Compteur)obj).clients.get(client)));
				}
				System.out.println("Rapport ----------->");
			}
		}
	}

}
