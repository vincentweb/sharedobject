
public interface SharedObject_itf2 extends SharedObject_itf {
	public int getId();
	public void setId(int id);
	public Object invalidate_writer();
	public void invalidate_reader();
	public Object reduce_lock();
}
