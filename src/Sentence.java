public class Sentence implements java.io.Serializable {

	private static final long serialVersionUID = -3727587107783900556L;

	String 		data;
	public Sentence() {
		data = new String("");
	}
	
	public void write(String text) {
		data += text+"\n"; // += car sinon on ne voit que la dernière écriture
	}
	public String read() {
		return data;
	}
	
	public String toString() {
		return data;
	}
	
}