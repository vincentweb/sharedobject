// Time-stamp: <26 Dec 2005 12:11 queinnec@enseeiht.fr>

import java.rmi.registry.*;

public class Afficheur {

    private static int NBVAR = 10;

    private static SharedObject get_object (String name, Object val) {
        SharedObject s = Client.lookup (name);
        if (s == null) {
            s = Client.create (val);
            Client.register (name, s);
        }
        return s;
    }

    public static void main(String argv[]) throws Exception {
        // initialize the system
        Client.init();

        SharedObject s_nbvar = get_object ("NBVAR", new Integer(NBVAR));
        s_nbvar.lock_write();
        int nbvar = ((Integer) s_nbvar.obj).intValue();
        s_nbvar.unlock();

        SharedObject somme = get_object ("Somme", new Integer(0));

        SharedObject[] lesVars = new SharedObject[nbvar];
        for (int i = 0; i < nbvar; i++) {
            lesVars[i] = get_object ("var"+i, new Integer(0));
        }

        while (true) {
            int sum = 0;
            System.out.print("0");
            for (int i = 0; i < nbvar; i++) {
                lesVars[i].lock_read();
                Integer val = (Integer) lesVars[i].obj;
                int v = val.intValue();
                lesVars[i].unlock();
                sum += v;
                System.out.print ("+" + v);
            }
            System.out.print ("= " + sum);
            somme.lock_read();
	    Thread.sleep(10);
            int v = ((Integer) somme.obj).intValue();
            System.out.println (" / les changeurs: " + v); 
            somme.unlock();
            
            Thread.sleep (1000);
        }
    }
}
