import java.awt.*;
import java.awt.event.*;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;


public class Irc extends Frame {

	private static final long serialVersionUID = 4392468196229185219L;

	public TextArea		 text;
	public TextField	 data;
	public SharedObject	 sentence;
	public static String myName;
	public static final Logger logger = Logger.getLogger("Irc.class");

	public static void main(String argv[]) {
		
		if (argv.length != 1) {
			System.out.println("java Irc <name>");
			return;
		}
		myName = argv[0];
	
		// initialize the system
		//Client.init("192.168.1.100");
		Client.init();
		Client.clientID += "_"+myName;
		
		// look up the IRC object in the name server
		// if not found, create it, and register it in the name server
		SharedObject s = Client.lookup("IRC");
		if (s == null) {
			s = Client.create(new Sentence());
			Client.register("IRC", s);
		}
		// create the graphical part
		new Irc(s);
	}

	@SuppressWarnings("deprecation")
	public Irc(SharedObject s) {
		DOMConfigurator.configure("log4j.xml");
		setLayout(new FlowLayout());
	
		text=new TextArea(10,60);
		text.setEditable(false);
		text.setForeground(Color.red);
		add(text);
	
		data=new TextField(60);
		add(data);
	
		Button write_button = new Button("write");
		write_button.addActionListener(new writeListener(this));
		add(write_button);
		Button read_button = new Button("read");
		read_button.addActionListener(new readListener(this));
		add(read_button);
		
		Button quit_button = new Button("quit");
		quit_button.addActionListener(new quitListener(this));
		add(quit_button);
		
		setSize(470,300);
		text.setBackground(Color.black); 
		show();
		
		sentence = s;
	}
}

class quitListener implements ActionListener {
	Irc irc;
	public quitListener (Irc i) {
		irc = i;
	}
	public void actionPerformed (ActionEvent e) {
		System.exit(0);
	}
}


class readListener implements ActionListener {
	Irc irc;
	public readListener (Irc i) {
		irc = i;
	}
	public void actionPerformed (ActionEvent e) {
		Irc.logger.trace(Irc.myName+" trying to get the read lock");
		
		// lock the object in read mode
		irc.sentence.lock_read();
		Irc.logger.trace(Irc.myName+" reading");
		
		// invoke the method
		String s = ((Sentence)(irc.sentence.obj)).read();
		Irc.logger.trace(Irc.myName+" unlocking.");
		
		// unlock the object
		irc.sentence.unlock();
		Irc.logger.trace(Irc.myName+" unlocked");
		
		// display the read value
		irc.text.append(s+"\n");
	}
}

class writeListener implements ActionListener {
	Irc irc;
	public writeListener (Irc i) {
        	irc = i;
	}
	public void actionPerformed (ActionEvent e) {
		Irc.logger.trace(Irc.myName+" trying to get the write lock");
		
		// get the value to be written from the buffer
        String s = irc.data.getText();
        
        
        // lock the object in write mode
		irc.sentence.lock_write();
		Irc.logger.trace(Irc.myName+" writing");
		
		// invoke the method
		((Sentence)(irc.sentence.obj)).write(Irc.myName+" wrote "+s);
		irc.data.setText("");
		Irc.logger.trace(Irc.myName+" unlocking.");
		
		// unlock the object
		irc.sentence.unlock();
		Irc.logger.trace(Irc.myName+" unlocked");
	}
}



