
public class DoubleWrite {

private static final long serialVersionUID = -4492550325490126607L;
	
	protected Integer cpt;
	
	public DoubleWrite(int init) {
		cpt = init;
	}

	public static void main(String[] args) {
		String addr = "127.10.0.1";
		if (args.length >= 1) {
			addr = args[0];
		}
		Client.init(addr);
		SharedObject s = Client.lookup("DoubleWrite");
		if (s == null) {
			s = Client.create(new Compteur(0));
			Client.register("CPT", s);
		}
		
		s.lock_read();
	
		try {
		} catch (Exception e) {
			e.printStackTrace();
		}
		s.unlock();
		
		Sleeping.sleep(2000);
		
		s.lock_write();
		Sleeping.sleep(2000);
		((Compteur)s.obj).cpt++;
		s.unlock();
		System.out.println("Fini");
		
	}
	

}
