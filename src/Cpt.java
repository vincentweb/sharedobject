public class Cpt implements java.io.Serializable{
    private int cpt;
    public Cpt(){
        this.cpt = 0;
    }
    public void inc(){
        this.cpt ++;
    }
    public int getCpt(){
        return this.cpt;
    }
}
