import java.util.Random ;
public class MauriceBe {
    private SharedObject cpt;
    private Random r ;

    public MauriceBe(SharedObject s) {
        this.cpt = s;
        this.r = new Random() ;
    }

    public static void main(String args[]){
        Client.init();
        SharedObject s = Client.lookup("cpt");
        if (s == null){
            System.out.println("Creation de l'objet") ;
            s = Client.create(new Cpt());        
            Client.register("cpt",s);
        }
        MauriceBe st = new MauriceBe(s);
        st.launch();
        System.out.println("A fini") ;
    }
    public void launch(){
        int j = 0 ;
        for (int i = 0;i < 10000;i++){
            System.out.println(i);
            if (this.r.nextInt(2) == 0) {
                System.out.println("J'ecris") ;
                cpt.lock_write();
                ((Cpt) cpt.obj).inc();
                cpt.unlock();
                j++;
            } else {
                System.out.println("Je lis") ;
                cpt.lock_read() ;
                int res = ((Cpt) cpt.obj).getCpt();
                cpt.unlock() ;
                System.out.println("Act :" + res);
            }
        }
        System.out.println("Resultat attendu : "+j) ;
        cpt.lock_read() ;
        int res = ((Cpt) cpt.obj).getCpt();
        cpt.unlock() ;
        System.out.println("Finalement :" + res);
        
    }
}
