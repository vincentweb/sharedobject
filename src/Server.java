import java.net.InetAddress;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * This class defines methods used to manage the shared objects on server side, and also creates the rmiregistry.
 * 
 * @author Jérémy RIVIERE
 * @author Vincent ANGLADON
 *
 * @see Server_itf
 */

public class Server extends UnicastRemoteObject implements Server_itf {

	protected static final long serialVersionUID = 7664908919577729878L;
	
	/**
	 * The counter used to generate a unique id for each object registered in the registry.
	 */
	public static int id = 0;
	
	/**
	 * Matches the object's id to its name.
	 */
	protected HashMap<String, Integer> registry;
	
	/**
	 * Matches a unique id to each object in the registry.
	 */
	protected HashMap<Integer, ServerObject> idmap;
	
	protected HashMap<Integer, Lock> locks;
	
	
	/**
	 * Ensures mutual exclusion in object creation.
	 */
	protected Semaphore createLock;
	
	/**
	 * Ensures mutual exclusion in object registering.
	 */
	protected Semaphore registerLock;
	
	/**
	 * Logger for Server.
	 */
	protected static final Logger logger = Logger.getLogger("Server.class");
	
	protected static Server_itf server;
	
	/**
	 * Creates a new server for the shared objects.
	 * @throws RemoteException
	 */
	public Server() throws RemoteException {
		super(); 
		registry = new HashMap<String, Integer>();
		idmap = new HashMap<Integer, ServerObject>();
		createLock = new Semaphore(1, false); 
		registerLock = new Semaphore(1, false);
		DOMConfigurator.configure("log4j.xml");
		locks = new HashMap<Integer, Lock>();
	}
	
	/**
	 * 
	 * Called by the client to look up the name server.
	 * @param name The name of the object the client wants to work on.
	 * @return The id associated to the given name in the registry, or -1 if not found.
	 */
	@Override
	public int lookup(String name) throws RemoteException {
		try {
			// One can not read the registry if it is currently being written.
			registerLock.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
			throw new RemoteException("lookup lock fail");
		}
		
		if (registry.containsKey(name)) {
			registerLock.release();
			System.out.println(String.format("New Client connected (%d)", idmap.size()));
			return registry.get(name);
		}
		else  {
			registerLock.release();
			return -1;
		}
		
	}
	
	/**
	 * 
	 * Called by the client to request the creation of a new object in the registry.
	 * @param o The object to be registered.
	 * @return The id associated to the given object.
	 */
	@Override
	public int create(Object o) throws RemoteException {
		try {
			createLock.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
			throw new RemoteException("Create lock fail");
		}
		System.out.println("New Client connected");
		ServerObject obj = new ServerObject(id, o);
		idmap.put(id, obj);
		logger.trace(String.valueOf(id) + " saved.");
		createLock.release();
		locks.put(id, new ReentrantLock());
		return id++;
	}

	/**
	 * 
	 * Called by the client to request the recording of a shared object in the registry.
	 * @param name The name to be given to the object.
	 * @param id The id associated to the object.
	 */
	@Override
	public void register(String name, int id) throws RemoteException {
		try {
			// One can not call register if someone else calls lookup/register.
			registerLock.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
			throw new RemoteException("Register lock fail");
		}
		
		if (registry.containsKey(name)) {
			// Two rival lookup returned null, therefore two objects were created with the same name.
			logger.warn(String.format("Doublon entre %d et %d", id, registry.get(name)));
			idmap.remove(id);
			registerLock.release();
			throw new RemoteException("Doublon");
			
		}
		
		registry.put(name, id);
		logger.trace("Mapped " + name +" with "+ String.valueOf(id));
		registerLock.release();
	}

	/**
	 * 
	 * Called by the client to request a new read lock on a shared object.
	 * @param id The id of object on which we want a read lock.
	 * @param client The client requesting the read lock
	 * @return The object as known by the server, or null if no id matches.
	 */
	@Override
	public Object lock_read(int id, Client_itf client) throws RemoteException {
//		logger.trace(String.format("%d : lock_read (%s)", id, client.getId()));
		ServerObject obj = idmap.get(id);
		if (obj == null) {
			logger.error(String.format("Server Error: Unknown object id: %d (lock_read)", id));
			return null;
		}
		else {
			return obj.lock_read(client);
		}
	}

	/**
	 * 
	 * Called by the client to request a new write lock on a shared object.
	 * @param id The id of object on which we want a write lock.
	 * @param client The client requesting the write lock
	 * @return The object as known by the server, or null if no id matches.
	 */
	@Override
	public Object lock_write(int id, Client_itf client) throws RemoteException {
//		logger.trace(String.format("%d : lock_write (%s)", id, client.getId()));
		locks.get(id).lock();
		ServerObject obj = idmap.get(id);
		if (obj == null) {
			logger.error(String.format("Server Error: Unknown object id: %d (lock_write)", id));
			locks.get(id).unlock();
			return null;
		}
		else {
//			synchronized (obj) {
				Object objet = obj.lock_write(client);
				locks.get(id).unlock();
				return objet;
//			}
		}
	}
	
	protected static void init(String[] args) {
		try {
			String addr = InetAddress.getLocalHost().getHostName();
			int port = 1099;
			
			if (args.length>1) {
				addr = args[0];
				port = Integer.parseInt(args[1]);
			}
			else if (args.length>0) {
				addr = args[0];
			}
			
			// Creation of the rmiregistry
			LocateRegistry.createRegistry(port);
			
			
			// Registering the server in the rmiregistry
			String url = "rmi://" + addr + ":" + String.valueOf(port) + "/SOserver";
			Naming.bind(url, server);
			System.out.println("Server online on "+addr);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		// Creation of the server instance used to handle shared objects
		try {
			server = new Server();
			init(args);
		} catch (RemoteException e) {
			e.printStackTrace();
		}				
	}

}
