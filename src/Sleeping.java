import java.util.Random;

public class Sleeping {
	
	/**
	 * Switch to quickly disable all the Thread.Sleep 
	 */
	public static boolean enabled = true;
	
	public static void sleep(int milliSec) {
		if (enabled) {
			try {
				Thread.sleep(milliSec);
			} catch (InterruptedException e) {
				System.err.println("Unable to sleep.");
				e.printStackTrace();
			}
		}
	}
	
	public static void randomSleep(int minMilliSec, int maxMilliSec, boolean print) {
		if (enabled) {
			Random rand = new Random();
			int time = rand.nextInt(maxMilliSec-minMilliSec)+minMilliSec;
			sleep(time);
			if (print)
				System.out.println("Sleeping for " + time + "ms");
		}
	}
}
