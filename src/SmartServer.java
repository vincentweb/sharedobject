import java.net.InetAddress;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.concurrent.Semaphore;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * This class defines methods used to manage the shared objects on server side, and also creates the rmiregistry.
 * 
 * @author Jérémy RIVIERE
 * @author Vincent ANGLADON
 *
 * @see Server_itf
 */

public class SmartServer extends Server{
	

	private static final long serialVersionUID = 1465090296929123098L;
	protected Object obj;
	CheckCounter cc;
	
	public SmartServer() throws RemoteException {
		super();
		cc = new CheckCounter();
		cc.start();
	}
	
	@Override
	public Object lock_read(int id, Client_itf client) throws RemoteException {
		Object obj2 = super.lock_read(id, client);
		if (registry.get("CPT").equals(id)) {
			obj = obj2;
			cc.updateObject(obj);
		}
		return obj2;
	}
	
	
	@Override
	public Object lock_write(int id, Client_itf client) throws RemoteException {
		
		Object obj2 = super.lock_write(id, client);
		if (registry.get("CPT").equals(id)) {
			obj = obj2;
			cc.updateObject(obj);
		}
		return obj2;
	}
	
	public static void main(String[] args) {
		// Creation of the server instance used to handle shared objects
		try {
			server = new SmartServer();
			init(args);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}				
	}
}
