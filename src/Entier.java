import java.io.Serializable;


public class Entier implements Serializable{
	
	private static final long serialVersionUID = -8802191874266753865L;
	private int nb;
	
	public Entier() {
		nb = 0;
	}
	
	public int read() {
		return nb;
	}
	
	public void add(int i) {
		nb += i;
	}
	
	public void set(int value) {
		nb = value;
	}
	
	public String toString() {
		return String.valueOf(nb);
	}
}
