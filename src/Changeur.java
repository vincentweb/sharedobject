// Time-stamp: <17 Jan 2006 16:27 queinnec@enseeiht.fr>

import java.util.Random;
import java.rmi.registry.*;

public class Changeur {

    private static int NBCHANGES = 500;

    private static SharedObject get_object (String name) {
        SharedObject s = Client.lookup (name);
        if (s == null) {
            System.err.println ("Demarrer d'abord un Afficheur.");
            System.exit(1);
        }
        return s;
    }

    public static void main(String argv[]) throws Exception {
        // initialize the system
        Client.init();

        SharedObject s_nbvar = get_object ("NBVAR");
        s_nbvar.lock_read();
        int nbvar = ((Integer) s_nbvar.obj).intValue();
        s_nbvar.unlock();

        SharedObject somme = get_object ("Somme");

        SharedObject[] lesVars = new SharedObject[nbvar];
        for (int i = 0; i < nbvar; i++) {
            lesVars[i] = get_object ("var"+i);
        }

        Random r = new Random();
        for (int n = NBCHANGES; n > 0; n--) {
            int i = r.nextInt (nbvar);
            int add = r.nextInt (10) + 1;
            System.out.print("+"+add);

            lesVars[i].lock_write();
            Integer val = (Integer) lesVars[i].obj;
            lesVars[i].obj = new Integer (val.intValue() + add);
            lesVars[i].unlock();

            somme.lock_write();
            Integer s = (Integer) somme.obj;
            somme.obj = new Integer (s.intValue() + add);
            somme.unlock();

            if (r.nextInt(10) > 1)  // 8 times upon 10
              Thread.sleep (10);
        }
        System.out.println ("!");
    }
}
