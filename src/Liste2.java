import java.io.Serializable;


public class Liste2 implements Serializable{
	
	private static final long serialVersionUID = 4162415824020581956L;
	private Entier[] liste;
	private boolean is_initialised;
	
	public Liste2(int size) {
		liste = new Entier[size];
		is_initialised = false;
	}
	
	public Entier get(int i){
		return liste[i];
	}
	
	public void set(Integer i, Entier obj) {
		liste[i] = obj;
		is_initialised = true;
	}
	
	public boolean is_initialised() {
		return is_initialised;
	}
	
}
