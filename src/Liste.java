import java.io.Serializable;


public class Liste implements Serializable{
	
	private static final long serialVersionUID = 4162415824020581955L;
	private SharedObject[] liste;
	private boolean is_initialised;
	
	public Liste(int size) {
		liste = new SharedObject[size];
		is_initialised = false;
	}
	
	public SharedObject get(int i){
		return liste[i];
	}
	
	public void set(int i, SharedObject obj) {
		liste[i] = obj;
		is_initialised = true;
	}
	
	public boolean is_initialised() {
		return is_initialised;
	}
	
}
