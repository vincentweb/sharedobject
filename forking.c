#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>

/* Utilisation : ./forking "programme_a_executer argument1 argument2 ..."
 * Les guillemets sont importants
 * 
 * Utilisation plus intéressante dans deploy.sh
 * Exemple : ssh -o ... blabla "./forking \"programme_a_executer\""
 *
 * But : rattacher programme_a_executer à init de sortes que si on perd 
 * la connection ssh ou qu'on la ferme volontairement 
 * alors le programme continu quand même de tourner
 * */

int main (int argc, char* argv[])
{
    int pid = fork();
    if (pid == 0)
    {
        execlp("bash", "bash", "-c", argv[1], NULL);
        exit(1);
    }
    else
    {
        wait(pid);
        exit(0);
    }
    return 0;
}
